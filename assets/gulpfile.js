'use strict';

const os = require('os');
const gulp = require('gulp');
const open = require('gulp-open');
const sass = require('gulp-sass');
const print = require('gulp-print');
const fs = require('fs');

const browser = os.platform() === 'linux' ? 'google-chrome' : (
    os.platform() === 'darwin' ? 'google chrome' : (
    os.platform() === 'win32' ? 'chrome' : 'firefox'));

gulp.task('loadPage', function(cb){
    gulp.src('../index.html')
    .pipe(open({app: browser}));
    fs.access('gulpfile.js', cb);
});

gulp.task('sass', function(){
return gulp.src('scss/main.scss')
    .pipe(sass())
    .pipe(gulp.dest('css/'));
});

gulp.task('watch', function(){
    gulp.watch('scss/**/*.scss', gulp.series('sass'));
});
