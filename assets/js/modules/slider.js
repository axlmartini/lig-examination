"use strict";

//global vars
var dotPosition = 0;
var sliderWidth = $(window).width();
var sliderHeight = null;
var numberOfSliders = null;
var hiddenSliderWidth = null;

$(document).ready(function(e){
    $('.slider__arrow--left').addClass('disabled'); //disable left arrow
    sliderHeight = $('.slider__item').height();
    numberOfSliders = $('.slider__item').length;
    hiddenSliderWidth = numberOfSliders * sliderWidth;
    $('.slider__viewport').css('height', sliderHeight+'px'); //add height to the display div
    $('.slider__items-holder').css('width', hiddenSliderWidth+'px'); //add width to the div that holds the slider items
    $('.slider__item').css({'width' : sliderWidth+'px', 'height' : sliderHeight+'px'}); //add width to each slider item
    
    //slider arrows
    $('.slider__arrow').on('click', function(e){
        e.preventDefault();
        var target = $(this).data('target');
        
        if(target == "next" && dotPosition < (numberOfSliders - 1)) { //limit to 2 actions
            dotPosition += 1;
        } else if(target == "prev" && dotPosition > 0) {
            dotPosition -= 1;
        }
        renderSlide();
    });
    
    //slider indicators
    $('.slider__indicator-item').on('click', function(e){
        e.preventDefault();
        var dotIndex = $(this).data('index'); //get dot index and assign to global dotPosition
        dotPosition = dotIndex;
        renderSlide();
    });
});

function renderSlide(){
    var position = (dotPosition != 0) ? (sliderWidth * dotPosition) * -1 : 0; //get needed 'left' value
    $('.slider__items-holder').css('left', position + 'px'); //show current slide
    $('.slider__indicator-item').removeClass('slider__indicator-item--active'); //remove active dot
    $('#sliderIndicator-' + dotPosition).addClass('slider__indicator-item--active'); //update active dot

    $('.slider__arrow--left').removeClass('disabled');
    $('.slider__arrow--right').removeClass('disabled');
    if(dotPosition == 0) {
        $('.slider__arrow--left').addClass('disabled');
    } else if(dotPosition == (numberOfSliders - 1)) {
        $('.slider__arrow--right').addClass('disabled');
    }
}