var _websiteLogo = {
    url: '#',
    imageSrc: 'assets/images/website-logo.png',
    alt: 'Website Logo'
};

var _socialMedias = [
    {
        url: 'https://www.instagram.com',
        imageSrc: 'assets/images/i-instagram.png',
        alt: 'Instagram'
    },
    {
        url: 'https://www.facebook.com',
        imageSrc: 'assets/images/i-facebook.png',
        alt: 'Facebook'
    },
    {
        url: 'https://www.twitter.com',
        imageSrc: 'assets/images/i-twitter.png',
        alt: 'Twitter'
    }
];

var _menuItems = [
    {
        url: '#',
        label: 'メニュー1',
        isActive: true,
    },
    {
        url: '#',
        label: 'メニュー2',
        isActive: false,
    },
    {
        url: '#',
        label: 'メニュー3',
        isActive: false,
    },
    {
        url: '#',
        label: 'メニュー4',
        isActive: false,
    },
    {
        url: '#',
        label: 'メニュー5',
        isActive: false,
    },
    {
        url: '#',
        label: 'メニュー6',
        isActive: false,
    },
];

var _sliderItems = [
    {
        imageSrc: 'assets/images/banner-item.jpg',
        heading: 'キャッチコピーが入ります。',
        description: ['サンプルテキストサンプルテキストサンプルテキストサンプルテキスト','サンプルテキストサンプルテキストサンプルテキストサンプルテキスト']
    },
    {
        imageSrc: 'assets/images/banner-item-2.jpg',
        heading: 'キャッチコピーが入ります。',
        description: ['サンプルテキストサンプルテキストサンプルテキストサンプルテキスト','サンプルテキストサンプルテキストサンプルテキストサンプルテキスト']
    },
    {
        imageSrc: 'assets/images/banner-item-3.jpg',
        heading: 'キャッチコピーが入ります。',
        description: ['サンプルテキストサンプルテキストサンプルテキストサンプルテキスト','サンプルテキストサンプルテキストサンプルテキストサンプルテキスト']
    }
];

var _sliderNavs = [
    {
        imageSrc: 'assets/images/prev_icon.png',
        alt: 'Previous item',
        class: 'slider__arrow--left',
        target: 'prev'
    },
    {
        imageSrc: 'assets/images/next_icon.png',
        alt: 'Next item',
        class: 'slider__arrow--right',
        target: 'next'
    }
];

var _articleItems = [
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        imageSrcAlt: 'Article image',
        imageSrcHover: 'assets/images/article-item-hover.jpg',
        imageSrcHoverAlt: 'Article image hover',
        date: '2016.01.01',
        heading: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト…'
    },
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        imageSrcAlt: 'Article image',
        imageSrcHover: 'assets/images/article-item-hover.jpg',
        imageSrcHoverAlt: 'Article image hover',
        date: '2016.01.01',
        heading: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト…'
    },
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        imageSrcAlt: 'Article image',
        imageSrcHover: 'assets/images/article-item-hover.jpg',
        imageSrcHoverAlt: 'Article image hover',
        date: '2016.01.01',
        heading: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト…'
    },
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        imageSrcAlt: 'Article image',
        imageSrcHover: 'assets/images/article-item-hover.jpg',
        imageSrcHoverAlt: 'Article image hover',
        date: '2016.01.01',
        heading: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト…'
    },
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        imageSrcAlt: 'Article image',
        imageSrcHover: 'assets/images/article-item-hover.jpg',
        imageSrcHoverAlt: 'Article image hover',
        date: '2016.01.01',
        heading: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト…'
    },
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        imageSrcAlt: 'Article image',
        imageSrcHover: 'assets/images/article-item-hover.jpg',
        imageSrcHoverAlt: 'Article image hover',
        date: '2016.01.01',
        heading: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト…'
    },
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        imageSrcAlt: 'Article image',
        imageSrcHover: 'assets/images/article-item-hover.jpg',
        imageSrcHoverAlt: 'Article image hover',
        date: '2016.01.01',
        heading: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト…'
    },
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        imageSrcAlt: 'Article image',
        imageSrcHover: 'assets/images/article-item-hover.jpg',
        imageSrcHoverAlt: 'Article image hover',
        date: '2016.01.01',
        heading: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト…'
    },
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        imageSrcAlt: 'Article image',
        imageSrcHover: 'assets/images/article-item-hover.jpg',
        imageSrcHoverAlt: 'Article image hover',
        date: '2016.01.01',
        heading: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト…'
    },
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        imageSrcAlt: 'Article image',
        imageSrcHover: 'assets/images/article-item-hover.jpg',
        imageSrcHoverAlt: 'Article image hover',
        date: '2016.01.01',
        heading: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト…'
    }
];

var _recommendItems = [
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        alt: 'Recommend image',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        date: '2016.03.26'
    },
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        alt: 'Recommend image',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        date: '2016.03.26'
    },
    {
        url: '#',
        imageSrc: 'assets/images/article-item-1.jpg',
        alt: 'Recommend image',
        description: 'サンプルテキストサンプルテキストサンプルテキストサンプルテキスト',
        date: '2016.03.26'
    }
];

var _dummyBannerItems = [
    {
        url: '#',
        description: 'Dummy banner'
    },
    {
        url: '#',
        description: 'Dummy banner'
    },
    {
        url: '#',
        description: 'Dummy banner'
    }
];

var _footerItems = [
    {
        url: '#',
        label: '運営会社'
    },
    {
        url: '#',
        label: '個人情報保護について'
    }
];