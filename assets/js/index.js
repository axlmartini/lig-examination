var websiteLogo = new Vue({
    el: '#websiteLogo',
    data: {
        websiteLogo: _websiteLogo
    },
});

var socialMedia = new Vue({
    el: '#socialMedia',
    data: {
        socialMedias: _socialMedias
    }
});

var mainMenu = new Vue({
    el: '#menuItems',
    data: {
        menuItems: _menuItems
    }
});

var slider = new Vue({
    el: '#sliderItems',
    data: {
        sliderItems: _sliderItems
    }
});

var sliderArrows = new Vue({
    el: '#sliderNavs',
    data: {
        sliderNavs: _sliderNavs
    }
});

var slider = new Vue({
    el: '#sliderDots',
    data: {
        sliderItems: _sliderItems
    }
});

var article = new Vue({
    el: '#articleItems',
    data: {
        articleItems: _articleItems
    }
});

var recommend = new Vue({
    el: '#recommendItems',
    data: {
        recommendItems: _recommendItems
    }
});

var dummyBanner = new Vue({
    el: '#dummyBannerItems',
    data: {
        dummyBannerItems: _dummyBannerItems
    }
});

var footer = new Vue({
    el: '#footerItems',
    data: {
        footerItems: _footerItems
    }
});