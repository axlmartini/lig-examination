# LIG examination

* SMACSS (file structure)
* BEM (css architecture)
* SASS
* jQuery
* Vue.js
* Gulp (compiler)
 

## Steps

* Clone project repo

>  `git clone git@gitlab.com:axlmartini/lig-examination.git`

* Run project options:
 

> Open `index.html` in your browser

or `(Note: must have gulp installed globally)`

> Open Command line (or any terminal) <br />
cd to project directory <br />
run: npm install <br />
cd assets <br />
run: gulp loadPage <br />


